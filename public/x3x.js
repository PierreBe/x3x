'use strict'
document.addEventListener('DOMContentLoaded', main)

let camera, scene, renderer
let cube

function main() {
    document.removeEventListener('DOMContentLoaded', main)
    init()
}

function init() {
    camera = new THREE.PerspectiveCamera(
        70,
        window.innerWidth / window.innerHeight,
        0.01,
        10
    )
    // camera.position.z = 1
    camera.position.set(1.5, 1.5, 1.5)
    camera.lookAt(new THREE.Vector3(0.5, 0.5, 0.5))

    scene = new THREE.Scene()
    scene.background = new THREE.Color('grey')

    renderer = new THREE.WebGLRenderer({ antialias: true })
    renderer.setSize(window.innerWidth, window.innerHeight)
    document.body.appendChild(renderer.domElement)

    let orbit = new THREE.OrbitControls(camera, renderer.domElement)
    orbit.mouseButtons = {
        MIDDLE: THREE.MOUSE.DOLLY,
        RIGHT: THREE.MOUSE.ROTATE,
    }
    orbit.addEventListener('change', render)

    window.addEventListener('resize', onWindowResize, false)

    size.addEventListener('input', resizeCube)
    command.addEventListener('change', inputCommand)

    resizeCube({target: size})
}

class Rubiks {
    _setCube(size, x, y, z, ...colors) {
        let geometry = new THREE.BoxGeometry(size, size, size)
        let material = new THREE.MeshBasicMaterial({
            polygonOffset: true,
            polygonOffsetFactor: 1,
            polygonOffsetUnits: 1,
        })
        for (let i = 0; i < 6; i++) {
            geometry.faces[2 * i].color.set(colors[i])
            geometry.faces[2 * i + 1].color.set(colors[i])
        }
        material.vertexColors = true
        let mesh = new THREE.Mesh(geometry, material)
        // mesh.geometry.translate(x, y, z)
        mesh.position.x = x
        mesh.position.y = y
        mesh.position.z = z
        // geometry.applyMatrix4(new THREE.Matrix4().makeTranslation(x, y, z))

        geometry = new THREE.EdgesGeometry(mesh.geometry)
        material = new THREE.LineBasicMaterial({
            color: 'black',
            // linewidth: size / 10, // Due to limitations of the OpenGL Core Profile with the WebGL renderer on most platforms linewidth will always be 1 regardless of the set value.
        })
        mesh.add(new THREE.LineSegments(geometry, material))
        return mesh
    }

    constructor(disc=3, size=1) {
        this.disc = disc
        this.size = size
        this.cubeSize = size / disc
        this.center = new THREE.Vector3(1/disc, 1/disc, 1/disc)
        this.cubes = Array(Math.pow(disc, 3) - Math.pow(disc - 2, 3))
        
        function isOnSide(...dims) {
            for(let dim of dims)
                if(dim == 0 || dim == disc - 1)
                // if(dim % (disc - 1) == 0)
                    return true
            return false
        }
        let acc = 0
        for (let z = 0; z < disc; z++)
            for (let y = 0; y < disc; y++)
                for(let x = 0; x < disc; x++)
                    if (isOnSide(x, y, z))
                        this.cubes[x + y * disc + z * Math.pow(disc, 2) - acc] = this._setCube(
                            this.cubeSize,
                            this.cubeSize * x,
                            this.cubeSize * y,
                            this.cubeSize * z,
                            x == disc - 1 ? 'red' : 'black',
                            x == 0 ? 'orange' : 'black',
                            y == disc - 1 ? 'yellow' : 'black',
                            y == 0 ? 'white' : 'black',
                            z == disc - 1 ? 'blue' : 'black',
                            z == 0 ? 'green' : 'black'
                        )
                    else
                        acc++
    }

    setScene(scene) {
        for (let cube of this.cubes) scene.add(cube)
    }

    unsetScene(scene) {
        for (let cube of this.cubes) scene.remove(cube)
    }

    _rotate(axis, clockwise, cubes) {
        let div = 128
        div = (Math.PI / 2 / div) * (clockwise ? 1 : -1)
        
        // animation
        console.log(cubes)

        for(let cube of cubes) {
            // console.groupCollapsed(cube)
            // console.log(cube.position.x, cube.position.y, cube.position.z)
            cube.parent.localToWorld(cube.position)
            cube.position.sub(this.center)
            cube.position.applyAxisAngle(axis, (Math.PI / 2) * (clockwise ? 1 : -1))
            cube.position.add(this.center)
            cube.parent.worldToLocal(cube.position)
            cube.rotateOnAxis(axis, (Math.PI / 2) * (clockwise ? 1 : -1))
            // console.log(cube.position.x, cube.position.y, cube.position.z)
            // console.groupEnd()
        }
        render()
    }

    rotate(command) {
        let axis, clockwise = command.length == 2 && command[1] == '\'', cubes
        switch(command[0]) {
            case 'R':
                axis = new THREE.Vector3(1, 0, 0)
                // cubes = this.cubes.filter((v, i, a) => v.position.x == this.cubeSize * (this.disc - 1))
                cubes = this.cubes.filter((v, i, a) => Math.abs(v.position.x - this.cubeSize * (this.disc - 1)) < Number.EPSILON*10)
                break
            case 'U':
                axis = new THREE.Vector3(0, 1, 0)
                // cubes = this.cubes.filter((v, i, a) => v.position.y == this.cubeSize * (this.disc - 1))
                cubes = this.cubes.filter((v, i, a) => Math.abs(v.position.y - this.cubeSize * (this.disc - 1)) < Number.EPSILON*10)
                break
            case 'F':
                axis = new THREE.Vector3(0, 0, 1)
                // cubes = this.cubes.filter((v, i, a) => v.position.z == this.cubeSize * (this.disc - 1))
                cubes = this.cubes.filter((v, i, a) => Math.abs(v.position.z - this.cubeSize * (this.disc - 1)) < Number.EPSILON*10)
                break
            default:
                console.warn('invalid command : ' + command)
                return
        }
        this._rotate(axis, clockwise, cubes)
    }
}

function render() {
    renderer.render(scene, camera)
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    renderer.render(scene, camera)
}

function resizeCube(event) {
    if (!event.target.validity.valid) return
    if (cube) cube.unsetScene(scene)
    cube = new Rubiks(event.target.value)
    cube.setScene(scene)
    render()
}

function inputCommand(event) {
    // console.log(cube.cubes[0])
    cube.rotate(event.target.value)
    event.target.value = ''
}